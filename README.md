# Le gîte du millepertuis

Source code of the site "[Le gîte du millepertuis](http://le-gite-du-millepertuis.fr/)".

## Production launch
Compilation : `npm run build`

Directories and files to be transferred :
- /includes/
- /public/
- /favicon.png
- /aide-ue.php
- /a-proximite.php
- /index.php
- /localisation.php
- /photo.php
- /tarif.php
- /.htaccess

The .htaccess file must be uncommented (activate the https redirection)
