/*
 * Scss
 */
import './scss/app.scss';

/*
 * Bootstrap JS
 */
import 'bootstrap/js/dist/collapse';
import 'bootstrap/js/dist/carousel';
import 'bootstrap/js/dist/util';

/*
 * Perso JS
 */
import './ts/components/contact';