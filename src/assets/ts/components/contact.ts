// Tel
let tel = ['4', '73', '21', '53', '16'];
let telLinkArray: NodeListOf<HTMLLinkElement> = document.querySelectorAll('.js-tel-link');
telLinkArray.forEach((telLink) => {
    telLink.innerHTML = '0' + tel.join(' ');
    telLink.href = 'tel:+33' + tel.join('');
});

// Email
let mail = ['contact', '@', 'le-gite-du-millepertuis', '.', 'fr'];
let mailLinkArray: NodeListOf<HTMLLinkElement> = document.querySelectorAll('.js-mail-link');
mailLinkArray.forEach((mailLink) => {
    mailLink.innerHTML = mail.join('');
    mailLink.href = 'mailto:' + mail.join('');
});