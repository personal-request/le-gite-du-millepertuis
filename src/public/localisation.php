<?php
$localisationActive = ' active';
?>

<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
              integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
              crossorigin=""/>
        <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/leaflet.fullscreen.css'
              rel='stylesheet' />

        <?php include './includes/css.php'; ?>
        <link rel="icon" type="image/png" href="favicon.png" />

        <title>Gîte du millepertuis, où nous trouver ? Comment venir ?</title>
        <meta name="description" content="Où se situe le Gîte du Millepertuis exactement ? Comment bien arriver à destination ?">
        <meta name="keywords" content="Gîte, Auvergne, Sancy, Chastreix, Ferme, Campagne, Emplacement, Carte, Lieu">

        <link rel="canonical" href="<?= ($_SERVER['HTTPS'] ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>" />
    </head>

    <body class="body">
        <?php include './includes/header.php'; ?>

        <main>
            <div class="container pt-5 px-4">
                <div class="row">
                    <div class="col-12 col-lg-9 mb-3 bg-white p-3">
                        <section>
                            <h1>Localisation</h1>
                            <div id="reboissonMap" class="localisation"></div>
                        </section>
                    </div>
                    <div class="d-none d-lg-block col-lg-3 mb-3 bg-white p-3">
                        <section>
                            <h2>Coordonées</h2>
                            <div class="card">
                                <div class="card-body">
                                    <address class="address">
                                        <div class="address__title">Adresse</div>
                                        <p>Le Gîte du Millepertuis<br/>Aline et Bruno BRUGIÈRE<br/>Reboisson<br/>63680 Chastreix</p>
                                        <div class="address__title">Téléphone</div>
                                        <p><a class="js-tel-link" href="#">04 XX XX XX XX</a></p>
                                        <div class="address__title">E-Mail</div>
                                        <p><a class="js-mail-link" href="#">xxxxx@xxx.xx</a></p>
                                    </address>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </main>

        <?php include './includes/footer.php'; ?>

        <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
                integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
        <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js'></script>

        <?php include './includes/js.php'; ?>

        <script>
                const latitude = 45.5145;
                const longitude = 2.7585;
                const zoom = 18;
                let reboissonMap = L.map('reboissonMap').setView([latitude, longitude], zoom);

                L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                    maxZoom: 19,
                    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                }).addTo(reboissonMap);

                reboissonMap.addControl(new L.Control.Fullscreen());
                const latitudeMarkerReboisson = 45.51445;
                const longitudeMarkerReboisson = 2.7588;
                let markerReboisson = L.marker([latitudeMarkerReboisson, longitudeMarkerReboisson]).addTo(reboissonMap);
                markerReboisson.bindPopup("Reboisson");

                // navigator.platform
                //
                // func navigateTo(latitude:Double, longitude:Double)
                // {
                //     let urlScheme = NSURL(string:"magicearth://")
                //
                //     if UIApplication.sharedApplication().canOpenURL(urlScheme!)
                //     {
                //         let string = String(format: "magicearth://?drive_to&lat=%f&lon=%f", arguments: [latitude, longitude])
                //
                //         let urlRoute = NSURL(string: string)
                //
                //         // Launch Magic Earth and start navigation.
                //         UIApplication.sharedApplication().openURL(urlRoute!)
                //     }
                // else
                //     {
                //         let urlStore = NSURL(string:"https://itunes.apple.com/app/id1007331679")
                //
                //         // Magic Earth is not installed. Launch AppStore.
                //         UIApplication.sharedApplication().openURL(urlStore!)
                //     }
                // }

        </script>
    </body>
</html>
