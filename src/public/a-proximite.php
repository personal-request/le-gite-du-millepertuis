<?php
$closeByActive = ' active';
?>

<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?php include './includes/css.php'; ?>
        <link rel="icon" type="image/png" href="favicon.png" />

        <title>Gîte du millepertuis, liste des activités et commerces aux alentours</title>
        <meta name="description" content="Quelques lieux utiles ou intéressants proches du Gîte du Millepertuis.">
        <meta name="keywords" content="Gîte, Auvergne, Sancy, Chastreix, Ferme, Campagne, Activités">

        <link rel="canonical" href="<?= ($_SERVER['HTTPS'] ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>" />
    </head>

    <body class="body">
        <?php include './includes/header.php'; ?>

        <main>
            <div class="container pt-5 px-4">
                <div class="row">
                    <div class="col-12 mb-3 bg-white p-3">
                        <section>
                            <h1>Lieux utiles pour le séjour</h1>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h2 class="card-title"><a href="https://www.chastreix.fr/" target="_blank">Chastreix</a> (2&nbsp;km) (<a href="https://www.sancy.com/decouvrir/toutes-les-communes/chastreix/" target="_blank">Commune de Chastreix</a>)</h2>
                                            <ul>
                                                <li>Alimentation</li>
                                                <li>Bureau de poste</li>
                                                <li><strong>Bureaux de la réserve naturelle</strong></li>
                                                <li>Café, bar</li>
                                                <li><a href="https://www.sancy.com" target="_blank">Office de tourisme</a></li>
                                                <li>Restauration rapide</li>
                                                <li><a href="https://www.sancy.com/decouvrir/incontournables/reserve-naturelle-chastreix-sancy/" target="_blank">Réserve naturelle nationale</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body">
                                            <h2 class="card-title">Chastreix-Sancy (8,5&nbsp;km)</h2>
                                            <ul>
                                                <li><strong>Chiens de traineau</strong>, activités en été (<a href="https://www.sancy.com/activite/randogs-chastreix/" target="_blank">Plus d'informations</a>)</li>
                                                <li><strong>Location de skis</strong>, de <strong>raquettes</strong>, etc</li>
                                                <li>Restaurants</li>
                                                <li><a href="https://www.sancy.com/decouvrir/toutes-les-communes/chastreix/chastreix-sancy-station-de-ski-et-sports-hiver/" target="_blank"><strong>Station de ski</strong></a> (Alpin et Fond)</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body">
                                            <h2 class="card-title">La Tour d'Auvergne (7&nbsp;km)</h2>
                                            <ul>
                                                <li>Bars</li>
                                                <li>Boucherie</li>
                                                <li>Boulangerie</li>
                                                <li>Crédit Agricole</li>
                                                <li><a href="https://www.rouillon-se-automobile.fr/" target="_blank">Garagiste</a></li>
                                                <li>Marché (Les Mardi et Samedi de 8h à 13h)</li>
                                                <li><strong>Médecin</strong></li>
                                                <li>Pharmacie</li>
                                                <li><strong>Plan d'eau</strong></li>
                                                <li>Restaurants</li>
                                                <li><strong>Station de ski de fond</strong> (La stèle&nbsp;-&nbsp;12&nbsp;km)</li>
                                                <li>Station essence</li>
                                                <li>Supérette (Vival)</li>
                                                <li>Tabac</li>
                                                <li>Magasins divers</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body">
                                            <h2 class="card-title">Tauves (15&nbsp;km)</h2>
                                            <ul>
                                                <li>Boucherie</li>
                                                <li>Boulangeries</li>
                                                <li>Fleuriste</li>
                                                <li><strong>Maison de l'<a href="https://artisatauves.monsite-orange.fr/page-5a2bfb40361ed.html" target="_blank">artisanat</a></strong></li>
                                                <li>Salle de spectacle (La Bascule)</li>
                                                <li>Magasins divers</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body">
                                            <h2 class="card-title">La Bourboule (19&nbsp;km)</h2>
                                            <ul>
                                                <li>Bars</li>
                                                <li><a href="https://www.cinebaroxy.fr/" target="_blank"><strong>Ciné</strong> Bar Roxy</a></li>
                                                <li>Distributeurs de billets</li>
                                                <li><strong>Marché de producteurs Bio</strong> (Les mardis matin de 8h à 13h, de Mai à Septembre)</li>
                                                <li><strong>Parc d'attractions</strong> en accès libre (<a href="https://www.parcfenestre.com/" target="_blank">Parc Fenestre</a>)</li>
                                                <li><a href="https://www.piscine-labourboule.fr/" target="_blank"><strong>Piscine</strong></a></li>
                                                <li>Restaurants</li>
                                                <li>Supermarchés</li>
                                                <li>Magasins divers</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h2 class="card-title">Le Mont-Dore (25&nbsp;km)</h2>
                                            <ul>
                                                <li>Centre équestre</li>
                                                <li><a href="https://lemontdore.fr/activites/parcours-dans-les-arbres">Parcours dans les arbres</a></li>
                                                <li><strong>Téléphérique</strong> (direction le Puy-de-Sancy)</li>
                                                <li>Magasins divers</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body">
                                            <h2 class="card-title">Picherande (11&nbsp;km)</h2>
                                            <ul>
                                                <li>Bars</li>
                                                <li>Boucherie</li>
                                                <li>Boulangerie</li>
                                                <li>Garagiste</li>
                                                <li>Loueurs de skis</li>
                                                <li>Plans d'eau</li>
                                                <li>Quincaillerie - Alimentation</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body">
                                            <h2 class="card-title">Besse-et-Saint-Anastaise (27&nbsp;km)</h2>
                                            <ul>
                                                <li>Centre équestre</li>
                                                <li>Restaurants</li>
                                                <li>Supermarché</li>
                                                <li>Magasins divers</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body">
                                            <h2 class="card-title">Super-Besse (23&nbsp;km)</h2>
                                            <ul>
                                                <li>Jeux pour enfants</li>
                                                <li><strong>Piscine</strong></li>
                                                <li>Plan d'eau</li>
                                                <li>Restaurants</li>
                                                <li><strong>Station de ski</strong> (Alpin et Fond)</li>
                                                <li>Tyrolienne</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body">
                                            <h2 class="card-title">Bort-les-Orgues (32&nbsp;km)</h2>
                                            <ul>
                                                <li><a href="http://www.bort-les-orgues.com/le-barrage/" target="_blank">Le <strong>barrage de bort</strong></a></li>
                                                <li><a href="http://www.bort-les-orgues.com/musee-de-la-tannerie-et-du-cuir/" target="_blank"><strong>Musée de la Tannerie et du Cuir</strong></a></li>
                                                <li><a href="https://www.facebook.com/vracabord/" target="_blank">Vrac à Bord</a> (<strong>Épicerie Vrac, Local & Bio</strong>)</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </main>

        <?php include './includes/footer.php'; ?>

        <?php include './includes/js.php'; ?>
    </body>
</html>
