<header>
    <div class="jumbotron">
        <div class="jumbotron__title">Le Gîte du Millepertuis</div>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Basculer la navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <?php $ariaCurrent = 'aria-current="page"' ?>

            <div class="collapse navbar-collapse" id="main-navbar">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item <?= $indexActive ?? '' ?>">
                        <a class="nav-link" href="/" <?= isset($indexActive) ? $ariaCurrent : '' ?>>Accueil</a>
                    </li>
                    <li class="nav-item <?= $priceActive ?? '' ?>">
                        <a class="nav-link" href="/tarif.php" <?= isset($priceActive) ? $ariaCurrent : '' ?>>Tarifs</a>
                    </li>
                    <li class="nav-item <?= $photoActive ?? '' ?>">
                        <a class="nav-link" href="/photo.php" <?= isset($photoActive) ? $ariaCurrent : '' ?>>Photos</a>
                    </li>
                    <li class="nav-item <?= $localisationActive ?? '' ?>">
                        <a class="nav-link" href="/localisation.php" <?= isset($localisationActive) ? $ariaCurrent : '' ?>>Localisation</a>
                    </li>
                    <li class="nav-item <?= $closeByActive ?? '' ?>">
                        <a class="nav-link" href="/a-proximite.php" <?= isset($closeByActive) ? $ariaCurrent : '' ?>>À proximité</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
