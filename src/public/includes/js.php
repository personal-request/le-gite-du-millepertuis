<script async type="text/javascript" src="build/app.f96a3316.js"></script>
<script async type="text/javascript" src="build/runtime.9d64b4e7.js"></script>

<?php
    if (in_array("$_SERVER[HTTP_HOST]", ['www.le-gite-du-millepertuis.fr', 'www.gite-du-millepertuis.fr', 'le-gite-du-millepertuis.fr', 'gite-du-millepertuis.fr'])) {
?>
    <script async src="https://umami.gabrugiere.net/script.js" data-website-id="1231f126-965e-4879-8919-3a5ad1c63e72"></script>
<?php
    }
?>
